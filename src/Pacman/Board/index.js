import React from 'react';
import PropTypes from 'prop-types';
import { BOARD_WIDTH, BOARD_HEIGHT } from '../constants';
import Walls from './Walls';
import WallsTest from './WallsTest';
import './style.scss';

export default function Board(props) {
    const { gridSize } = props;

    const boardWidth = gridSize * BOARD_WIDTH;
    const boardHeight = gridSize * BOARD_HEIGHT;
    // const boardWidth = 1080;
    // const boardHeight = 1038;
    // console.log('gridSize', gridSize);
    // <svg width={1080} height={1038} >

    const posY = -240;

    return (
        <div className="pacman-board">
            {/* <svg width={boardWidth} height={boardHeight}>
                <rect x={0} y={0} width={boardWidth} height={boardHeight} fill="#03141c" />
                <Walls {...props} />
            </svg> */}
            <svg viewBox={`${0} ${posY} ${1080} ${1038 - posY}`} width={boardWidth} height={boardHeight}>
                <rect x={0} y={0} width={boardWidth} height={boardHeight} fill="#03141c" />
                <WallsTest {...props} />
            </svg>
            {/* <svg viewBox={`${0} ${posY} ${1080} ${1038 - posY}`} >
                <rect x={0} y={0} width={1080} height={1038} fill="#03141c" />
                <WallsTest {...props} />
            </svg> */}
        </div>
    );
}

Board.propTypes = {
    gridSize: PropTypes.number.isRequired
};

